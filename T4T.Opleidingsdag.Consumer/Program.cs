﻿using System;
using SqlStreamStore;
using SqlStreamStore.Streams;

namespace T4T.Opleidingsdag.Consumer
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var store = new MsSqlStreamStore(new MsSqlStreamStoreSettings("Server=.;Database=T4T.Opleidingsdag.Oefening1;Trusted_Connection=True;")))
            {
                var streamId = new StreamId("Foo");

                var page = store.ReadStreamForwards(streamId, 0, 10).Result;

                foreach (var message in page.Messages)
                {
                    var jsonData = message.GetJsonData().Result;
                    Console.WriteLine($"{message.MessageId}: {message.Type}, {jsonData}");
                }
            }

            Console.ReadKey();
        }
    }
}
