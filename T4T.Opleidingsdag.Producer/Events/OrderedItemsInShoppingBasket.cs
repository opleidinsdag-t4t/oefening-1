﻿using System;
using System.Collections.Generic;
using System.Text;

namespace T4T.Opleidingsdag.Producer.Events
{
    public class OrderedItemsInShoppingBasket
    {
        public Guid ShoppingBasketId { get; set; }
    }
}
