﻿using System;
using Newtonsoft.Json;
using SqlStreamStore;
using SqlStreamStore.Streams;
using T4T.Opleidingsdag.Producer.Events;

namespace T4T.Opleidingsdag.Producer
{
    class Program
    {
        static void Main(string[] args)
        {
            var shoppingBasketCreated = new ShoppingBasketCreated
            {
                ShoppingBasketId = Guid.NewGuid(),
                UserId = Guid.NewGuid()
            };

            var shoppingBasketCreatedJsonData = JsonConvert.SerializeObject(shoppingBasketCreated);
            var shoppingBasketCreatedMsg = new NewStreamMessage(Guid.NewGuid(), typeof(ShoppingBasketCreated).FullName, shoppingBasketCreatedJsonData);

            var itemAdded = new ItemAddedToShoppingBasket
            {
                ItemId = Guid.NewGuid(),
                Quantity = 2,
                ShoppingBasketId = shoppingBasketCreated.ShoppingBasketId
            };
            var itemAddedJsonData = JsonConvert.SerializeObject(itemAdded);
            var itemAddedMsg = new NewStreamMessage(Guid.NewGuid(), typeof(ItemAddedToShoppingBasket).FullName, itemAddedJsonData);

            var orderedItem = new OrderedItemsInShoppingBasket
            {
                ShoppingBasketId = shoppingBasketCreated.ShoppingBasketId
            };
            var orderedItemJsonData = JsonConvert.SerializeObject(orderedItem);
            var orderedItemMsg = new NewStreamMessage(Guid.NewGuid(), typeof(OrderedItemsInShoppingBasket).FullName, orderedItemJsonData);

            var streamId = new StreamId("Foo");

            using (var store = new MsSqlStreamStore(new MsSqlStreamStoreSettings("Server=.;Database=T4T.Opleidingsdag.Oefening1;Trusted_Connection=True;")))
            {
                store.CreateSchema().Wait();

                store.AppendToStream(streamId, ExpectedVersion.Any, new[]
                {
                    shoppingBasketCreatedMsg,
                    itemAddedMsg,
                    orderedItemMsg
                });
            }
        }
    }
}
